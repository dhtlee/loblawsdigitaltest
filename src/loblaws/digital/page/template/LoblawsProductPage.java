package loblaws.digital.page.template;

import java.util.List;

import loblaws.digital.utils.WebElementUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class LoblawsProductPage extends Page
{
	@FindBy (css = "button[class*=add-to-cart]")
	private List<WebElement> addToCartButtons;
	
	@FindBy (css = "div[class='slot available']")
	private List<WebElement> availableSlots;
	
	@FindBy (css = "button[data-map-text='View options as a List']")
	private WebElement viewOptionsAsListButton;
	
	@FindBy (xpath = "//button[contains(text(),'Schedule Pick Up')]")
	private WebElement schedulePickupButton;
	
	@FindBy (xpath = "//button[contains(text(),'TAKE IT')]")
	private WebElement takeTimeSlotButton;
	
	@FindBy (css = "span[class*='store-name-text']")
	private WebElement storeName;
	
	@FindBy (css = "span[class*='pickup-time-text']")
	private WebElement pickupTime;
	
	public LoblawsProductPage(WebDriver driver)
	{
		super(driver);
	}

	public void clickAddToCart(int productNum) {
		WebElement productAddToCartButton = addToCartButtons.get(productNum);
		WebElementUtils.clickAjaxElement(driver, productAddToCartButton);
	}

	public void selectPickupLocation(String location) {
		WebElement selectButton = driver
				.findElement(By
						.cssSelector("div[class='wrapper-button visible-lg']>button[data-store-title='"
								+ location + "']"));
		WebElementUtils.clickAjaxElement(driver, selectButton);
	}

	public void clickViewOptionsAsList() {
		WebElementUtils.clickAjaxElement(driver, viewOptionsAsListButton);
	}
	
	public void selectTimeSlot(int slotNum)
	{
		WebElement timeslot = availableSlots.get(slotNum);
		WebElementUtils.clickAjaxElement(driver, timeslot);
	}
	
	public void clickSchedulePickUpButton()
	{
		WebElementUtils.clickAjaxElement(driver, schedulePickupButton);
	}
	
	public void clickTakeTimeslotButton()
	{
		WebElementUtils.clickAjaxElement(driver, takeTimeSlotButton);
	}
	
	public String getStoreName()
	{
		return storeName.getText().trim();
	}
	
	public String getPickupTime()
	{
		return pickupTime.getText().trim();
	}
}
