package loblaws.digital.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WindowUtils 
 {
	public static void waitForJavaScriptToComplete(WebDriver driver) {
		ExpectedCondition<Boolean> done = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				return (Boolean) js
						.executeScript("return (jQuery.active == 0) && $(\":animated\").length == 0");
			}
		};
		WebDriverWait waitForJs = null;
		waitForJs = new WebDriverWait(driver, 10000);
		waitForJs.until(done);
	}
}
