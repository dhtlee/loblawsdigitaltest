package loblaws.digital.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AutomationProperties {
	
	private static final String INIT_PROPERTIES = "init.properties";

	private static Properties getProperties(String fileName) throws IOException {
		InputStream stream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(fileName);
		if (stream == null) {
			throw new RuntimeException("File [" + fileName + "] does not exist!");
		}

		Properties properties = new Properties();
		properties.load(stream);
		return properties;
	}

	public static String getInitProperty(String key) throws IOException {
		String property = null;
		property = getProperties(INIT_PROPERTIES).getProperty(key);
		return property;
	}

}
