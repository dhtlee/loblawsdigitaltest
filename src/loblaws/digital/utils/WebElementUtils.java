package loblaws.digital.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class WebElementUtils 
{
	@SuppressWarnings("deprecation")
	public static void clickAjaxElement(WebDriver driver, WebElement element)
	{
		Actions builder = new Actions(driver);
		builder.click(element).pause(1500);
		builder.perform();
	}
}
