package loblaws.digital.test.template;

import java.io.IOException;

import loblaws.digital.utils.AutomationProperties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class LoblawsTestTemplate 
{
	protected WebDriver driver;

	@BeforeClass
	public void setupDriver() throws IOException {
		System.setProperty("webdriver.chrome.driver",
				AutomationProperties.getInitProperty("webdriver.chrome.driver"));

		driver = new ChromeDriver();
	}

	@BeforeClass(dependsOnMethods = { "setupDriver" })
	public void navigateToLandingPage() throws IOException {
		driver.get("https://shop.loblaws.ca/about");
	}

	@AfterClass
	public void cleanupDriver() {
		driver.quit();
	}
}
