package loblaws.digital.pages;

import loblaws.digital.page.template.Page;
import loblaws.digital.utils.WindowUtils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoblawsHomePage extends Page
{
	@FindBy (css = "input#search-bar")
	private WebElement searchBar;
	
	public LoblawsHomePage(WebDriver driver) throws InterruptedException
	{
		super(driver);
		WindowUtils.waitForJavaScriptToComplete(driver);
	}

	public LoblawsSearchResultPage search(String text)
	{
		searchBar.sendKeys(text + Keys.ENTER);
		return new LoblawsSearchResultPage(driver);
	}
}
