package loblaws.digital.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import loblaws.digital.page.template.Page;
import loblaws.digital.utils.WebElementUtils;
import loblaws.digital.utils.WindowUtils;

public class JoeFreshHomePage extends Page
{
	@FindBy (css = "a[title='Joe Fresh Home']")
	private WebElement joeFreshHomeLink;
	
	@FindBy (xpath = "//span[text()='SIGN UP + GET 10% OFF ONLINE']")
	private WebElement signUpGet10Off;
	
	@FindBy (id = "email")
	private WebElement emailTextField;
	
	@FindBy (id = "accept-terms-conditions")
	private WebElement acceptTermsConditions;
	
	@FindBy (xpath = "//div[contains(@class,'field-CA')]/button")
	private WebElement signupButton;
	
	@FindBy (xpath = "//button[contains(text(),'happy shopping')]")
	private WebElement happyShoppingButton;
	
	public JoeFreshHomePage(WebDriver driver) throws InterruptedException
	{
		super(driver);
		WindowUtils.waitForJavaScriptToComplete(driver);
		Thread.sleep(5000); // temp hack to make it work
	}
	
	public boolean isSignUpGet10OffDisplayed()
	{
		return signUpGet10Off.isDisplayed();
	}
	
	public void enterEmail(String email)
	{
		emailTextField.sendKeys(email + Keys.TAB);
	}
	
	// TODO: Need to account for the check and uncheck states for this checkbox as well
	public void acceptTermsConditions()
	{
		acceptTermsConditions.click();
	}
	
	public void clickSignupButton()
	{
		WebElementUtils.clickAjaxElement(driver, signupButton);
	}
	
	public void clickHappyShoppingButton()
	{
		WebElementUtils.clickAjaxElement(driver, happyShoppingButton);
	}
	
	public JoeFreshHomePage goBackToJoeFreshHome() throws InterruptedException
	{
		joeFreshHomeLink.click();
		return new JoeFreshHomePage(driver);
	}
}
