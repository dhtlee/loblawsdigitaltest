package loblaws.digital.pages;

import java.util.List;

import loblaws.digital.page.template.LoblawsProductPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoblawsSearchResultPage extends LoblawsProductPage
{
	@FindBy (css = "button[class*=add-to-cart]")
	private List<WebElement> addToCartButtons;
	
	public LoblawsSearchResultPage(WebDriver driver)
	{
		super(driver);
	}
	
	public int getNumDeals(String dealType)
	{
		return driver.findElements(By.xpath("//span[contains(text(),'" + dealType + "')]")).size();
	}
	
}
