package loblaws.digital.tests;

import loblaws.digital.pages.JoeFreshHomePage;
import loblaws.digital.test.template.JoeFreshTestTemplate;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Test1 extends JoeFreshTestTemplate
{
	@DataProvider(name = "testData")
	public static Object[][] data() {
		return new Object[][] {{ "test123@gmail.com"}};
	}

	@Test(dataProvider = "testData")
	public void firstTest(String email) throws InterruptedException {
		JoeFreshHomePage homepage = new JoeFreshHomePage(driver);
		Assert.assertTrue(homepage.isSignUpGet10OffDisplayed());
		
		homepage.enterEmail(email);
		homepage.acceptTermsConditions();
		homepage.clickSignupButton();
		homepage.clickHappyShoppingButton();
		
		// go back to homepage
		homepage = homepage.goBackToJoeFreshHome();
		Assert.assertFalse(homepage.isSignUpGet10OffDisplayed());
	}
}
