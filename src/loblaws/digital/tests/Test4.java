package loblaws.digital.tests;

import loblaws.digital.pages.LoblawsHomePage;
import loblaws.digital.pages.LoblawsSearchResultPage;
import loblaws.digital.test.template.LoblawsTestTemplate;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Test4 extends LoblawsTestTemplate
{
	@DataProvider(name = "testData")
	public static Object[][] data() {
		return new Object[][] {{ "butter", "LSL Leslie & Lakeshore"}};
	}

	@Test(dataProvider = "testData")
	public void firstTest(String product, String pickupLocation) throws InterruptedException {
		LoblawsHomePage homepage = new LoblawsHomePage(driver);

		LoblawsSearchResultPage searchResPage = homepage.search(product);

		searchResPage.clickAddToCart(0);
		searchResPage.clickViewOptionsAsList();
		searchResPage.selectPickupLocation(pickupLocation);
		searchResPage.clickSchedulePickUpButton();
		searchResPage.selectTimeSlot(0);  // just pick first timeslot for now
		searchResPage.clickTakeTimeslotButton();

		// refresh new instance of page object
		searchResPage = new LoblawsSearchResultPage(driver);
		Assert.assertTrue(searchResPage.getStoreName().equalsIgnoreCase(pickupLocation));
		
		//TODO: need to assert time slot as well
		
	}
}
