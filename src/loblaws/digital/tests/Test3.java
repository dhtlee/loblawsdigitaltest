package loblaws.digital.tests;

import loblaws.digital.pages.LoblawsHomePage;
import loblaws.digital.pages.LoblawsSearchResultPage;
import loblaws.digital.test.template.LoblawsTestTemplate;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Test3 extends LoblawsTestTemplate 
{
	@DataProvider(name = "testData")
	public static Object[][] data() {
		return new Object[][] {{ "butter", "SAVE $0.30", 5}};
	}

	@Test(dataProvider = "testData")
	public void firstTest(String product, String dealType, int expectedNum) throws InterruptedException {
		LoblawsHomePage homepage = new LoblawsHomePage(driver);

		LoblawsSearchResultPage searchResPage = homepage.search(product);
		Assert.assertEquals(expectedNum, searchResPage.getNumDeals(dealType));
	}
}
